
# README

##TourGuide

This app is an about improving the performance an API on an existing app called TourGuide.

## Technical:

1. TourGuide's app
   1. Framework: Spring Boot v2.1.6
   2. Framework: Spring Web
   3. FrameWork: SpringBootStarter
   4. Java 1.8
   5. Gradle 4.8.1
   6. Libs :
       1. gpsUtil
       2. RewardCentral
       3. TripPricer


## Instructions
1. Run *Application.java* via your IDE 

## Tourguide API Endpoints :
- host address :

  - ***localhost:8080*** (as 8080 is the default port, but it can be changed in application.properties file)
- list of endpoints :
  - ***/*** : This endpoint is the front page of the api.
  - ***/getLocation?userName=<Username>*** : This endpoint is used to track one user location.
  - ***/getNearbyAttractions?userName=<Username>*** : This endpoint is used to display for a specific user, the five closest attractions from this user, the longitude and latitude of each of these attractions, the longitude and latitude of the user, the distance in miles between the user and each attraction, and eventually the reward points given to the user if all these attractions are visited. 
  - ***/getAllCurrentLocations*** : This endpoint is used to give you all the last location of each user in the app's database.
  - ***/getRewards?userName=<Username>*** : This endpoint calculates reward for a specific user.
  - ***/getTripDeals?userName=<Username>*** : This endpoint is used to fetch trip deals from a user, thanks to its reward points and its parameters.
  - ***/getUser?userName=<Username>*** : This endpoint is used to display a user.
  - ***/updateUserPreferences?userName=<Username>*** : this endpoint is used to set a user's preferences.
  - ***/getUserPreference?userName=<Username>*** : this endpoint is used to get a user's preferences.
