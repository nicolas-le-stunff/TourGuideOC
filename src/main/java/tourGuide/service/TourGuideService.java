package tourGuide.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserLocation;
import tourGuide.user.UserNearAttractions;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserPreferencesDTO;
import tourGuide.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideService {
	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private GpsUtil gpsUtil;
	private RewardsService rewardsService;
	private TripPricer tripPricer = new TripPricer();
	public Tracker tracker;
	boolean testMode = true;

	ExecutorService executorService;

	public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService, ExecutorService... executor) {
		if (executor.length > 0) {
			this.executorService = executor[0];
		} else {
			this.executorService = Executors.newFixedThreadPool(10);
		}

		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;

		if (testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}

	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}

	public VisitedLocation getUserLocation(User user) throws InterruptedException, ExecutionException {
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ? user.getLastVisitedLocation()
				: trackUserLocation(user).get();
		return visitedLocation;
	}

	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}

	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}

	public void addUser(User user) {
		if (!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}

	public List<Provider> getTripDeals(User user) {
		int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
		List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(),
				user.getUserPreferences().getNumberOfAdults(), user.getUserPreferences().getNumberOfChildren(),
				user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}

	public Future<VisitedLocation> trackUserLocation(User user) {
		return executorService.submit(() -> {
			VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
			user.addToVisitedLocations(visitedLocation);

			return visitedLocation;
		});

	}

	public void trackUserLocations(User user) {
		VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
		user.addToVisitedLocations(visitedLocation);
		rewardsService.calculateRewards(user);

	}

	public void trackListUserLocation(List<User> userList) throws InterruptedException {
		for (User user : userList) {
			Runnable runnable = () -> {
				trackUserLocation(user);
			};
			executorService.execute(runnable);
		}
	}

	public List<UserNearAttractions> getNearByAttractions(VisitedLocation visitedLocation, User user) {
		List<UserNearAttractions> listNearAttractions = new ArrayList<>();
		List<Attraction> allAttractions = getAttractions();
		List<Future<UserNearAttractions>> futureListUserNearAttractions = new ArrayList<Future<UserNearAttractions>>();

		for (Attraction attraction : allAttractions) {
			Callable<UserNearAttractions> callable = () -> new UserNearAttractions(attraction.attractionName,
					attraction.latitude, attraction.longitude, visitedLocation.location,
					rewardsService.getDistance(attraction, visitedLocation.location),
					rewardsService.getRewardPoints(attraction, user));

			Future<UserNearAttractions> futureUserNearAttractions = executorService.submit(callable);
			futureListUserNearAttractions.add(futureUserNearAttractions);
		}
		for (Future<UserNearAttractions> future : futureListUserNearAttractions) {
			UserNearAttractions nearAttractions = null;
			try {
				try {
					nearAttractions = (UserNearAttractions) future.get();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			listNearAttractions.add(nearAttractions);
		}

		List<UserNearAttractions> listNearAttractionsSorted = listNearAttractions.stream()
				.sorted(Comparator.comparing(UserNearAttractions::getAttractionRange)).limit(5)
				.collect(Collectors.toList());
		return listNearAttractionsSorted;

	}

	public List<Attraction> getAttractions() {
		return gpsUtil.getAttractions();
	}

	public List<UserLocation> getAllCurrentLocations() {
		List<User> userList = getAllUsers();
		List<UserLocation> userLocation = new ArrayList<>();
		userList.forEach(user -> {
			userLocation.add(new UserLocation(user.getUserId(), user.getLastVisitedLocation().location));
		});
		return userLocation;
	}
	
	public UserPreferencesDTO getUserPreferences(String userName) {
		User user = getUser(userName);
		UserPreferences userPreferences = user.getUserPreferences();
		return new UserPreferencesDTO(userPreferences);
	}

	public void updatePreference(UserPreferencesDTO userPreference, String username) {
		User user = getUser(username);
		if (user != null) {
			user.getUserPreferences().setTicketQuantity(userPreference.getTicketQuantity());
			user.getUserPreferences().setNumberOfAdults(userPreference.getNumberOfAdults());
			user.getUserPreferences().setNumberOfChildren(userPreference.getNumberOfChildren());
			user.getUserPreferences().setTripDuration(userPreference.getTripDuration());
			internalUserMap.put(username, user);
		}
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				tracker.stopTracking();
			}
		});
	}

	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes
	// internal users are provided and stored in memory
	private final Map<String, User> internalUserMap = new HashMap<>();

	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);

			internalUserMap.put(userName, user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}

	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i -> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(),
					new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}

	private double generateRandomLongitude() {
		double leftLimit = -180;
		double rightLimit = 180;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
		double rightLimit = 85.05112878;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
		return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}

}
