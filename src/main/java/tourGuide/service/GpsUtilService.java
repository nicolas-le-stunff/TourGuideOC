package tourGuide.service;

import java.util.List;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;

public class GpsUtilService {

	private final GpsUtil gpsUtil;

	public GpsUtilService() {
		this.gpsUtil = new GpsUtil();
	}

	public List<Attraction> getAttractions() {
		return gpsUtil.getAttractions();
	}

}
