package tourGuide.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserNameNotFoundException extends RuntimeException {
	
	private final Logger logger = LoggerFactory.getLogger(UserNameNotFoundException.class);
	
	public UserNameNotFoundException(String userName) {
		logger.error("UserName was not found : "+userName);
	}
	
}
