package tourGuide.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tourGuide.service.TourGuideService;
@RestController
public class HomeController {
	
    private Logger logger = LoggerFactory.getLogger(HomeController.class);

	
	@Autowired
	TourGuideService tourGuideService;
	
	
    /** 
    * @return a string message
    */
	@RequestMapping("/")
	public String index() {
		logger.debug("Access / endpoint");
		return "Greetings from TourGuide!";
	}
}
