package tourGuide.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import tourGuide.exception.UserNameNotFoundException;
import tourGuide.service.TourGuideService;

@RestController
public class RewardsController {
	
	private Logger logger = LoggerFactory.getLogger(RewardsController.class);
	 
	@Autowired
	TourGuideService tourGuideService;
	
	/**
	 * @param UserName String
	 * @return rewards for this user
	 */
	@RequestMapping("/getRewards")
	public String getRewards(@RequestParam String userName) {
		logger.debug("Access to /getRewards with userName : "+userName);
		if(tourGuideService.getUser(userName)== null) {
			throw new UserNameNotFoundException(userName);
		}
		return JsonStream.serialize(tourGuideService.getUserRewards(tourGuideService.getUser(userName)));
	}
}
