package tourGuide.controller;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.VisitedLocation;
import tourGuide.exception.UserNameNotFoundException;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;

@RestController
public class LocationController {

    private Logger logger = LoggerFactory.getLogger(LocationController.class);
    
	@Autowired
	TourGuideService tourGuideService;

	/**
	 * @param UserName String
	 * @return Last user location
	 */
	@RequestMapping("/getLocation")
	public String getLocation(@RequestParam String userName) throws InterruptedException, ExecutionException {
		logger.debug("Access to /getLocation with userName : "+userName);
		if(getUser(userName)== null) {
			throw new UserNameNotFoundException(userName);
		}
		VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
		return JsonStream.serialize(visitedLocation.location);
	}
	/**
	 * @param UserName String
	 * @return a List of Attractions.
	 */
	@RequestMapping("/getNearbyAttractions")
	public String getNearbyAttractions(@RequestParam String userName) throws InterruptedException, ExecutionException {
		logger.debug("Access to /getNearbyAttractions with userName : "+userName);
		VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
		User user = tourGuideService.getUser(userName);
		if(user == null) {
			throw new UserNameNotFoundException(userName);
		}
		return JsonStream.serialize(tourGuideService.getNearByAttractions(visitedLocation, user));
	}



	// Get a list of every user's most recent location as JSON
	@RequestMapping("/getAllCurrentLocations")
	public String getAllCurrentLocations() {
		logger.debug("Access to /getAllCurrentLocations");
		return JsonStream.serialize(tourGuideService.getAllCurrentLocations());

	}
	
	private User getUser(String userName) {
		return tourGuideService.getUser(userName);
	}
}