package tourGuide.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import tourGuide.exception.UserNameNotFoundException;
import tourGuide.service.TourGuideService;
import tripPricer.Provider;

@RestController
public class TripController {
	
    private Logger logger = LoggerFactory.getLogger(TripController.class);
    
	@Autowired
	TourGuideService tourGuideService;
	
	@RequestMapping("/getTripDeals")
	public String getTripDeals(@RequestParam String userName) {
		logger.debug("Access to /getTripDeals with userName : "+userName);
		if(tourGuideService.getUser(userName)== null) {
			throw new UserNameNotFoundException(userName);
		}
		List<Provider> providers = tourGuideService.getTripDeals(tourGuideService.getUser(userName));
		return JsonStream.serialize(providers);
	}


}
