package tourGuide.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import tourGuide.exception.UserNameNotFoundException;
import tourGuide.service.TourGuideService;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserPreferencesDTO;

@RestController
public class UserController {
	
    private Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	TourGuideService tourGuideService;
	
	@PutMapping("/updateUserPreferences")
	public void updateUserPreferences(@RequestParam String userName, @RequestBody UserPreferencesDTO userPreferences) {
		logger.debug("Access to /updateUserPreferences with param : " +userName);
		tourGuideService.updatePreference(userPreferences, userName);
	}

	@RequestMapping("/getUserPreference")
	public String getUserPreferences(@RequestParam String userName) {
		logger.debug("Access to /getUserPreference with param : " +userName);
		UserPreferencesDTO userDTO = tourGuideService.getUserPreferences(userName);
		if(userDTO == null) {
			throw new UserNameNotFoundException(userName);	
			}
		return JsonStream.serialize(userDTO);
	}


}
