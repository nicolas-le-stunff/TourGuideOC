package tourGuide.user;

import java.util.UUID;

import gpsUtil.location.Location;

public class UserLocation {

	private UUID userUUID;
	private Location location;

	public UUID getUserUUID() {
		return userUUID;
	}

	public void setUserUUID(UUID userUUID) {
		this.userUUID = userUUID;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public UserLocation(UUID userUUID, Location location) {
		this.userUUID = userUUID;
		this.location = location;
	}
}
