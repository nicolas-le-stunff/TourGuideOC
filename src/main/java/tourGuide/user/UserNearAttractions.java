package tourGuide.user;

import gpsUtil.location.Location;

public class UserNearAttractions {

	private String attractionName;
	private Double attractionLattitude;
	private Double attractionLongitude;
	private Location location;
	private Double attractionRange;
	private int rewardsPoints;

	public UserNearAttractions(String attractionName, Double attractionLattitude, Double attractionLongitude,
			Location location, Double attractionRange, int rewardsPoints) {
		this.attractionName = attractionName;
		this.attractionLattitude = attractionLattitude;
		this.attractionLongitude = attractionLongitude;
		this.location = location;
		this.attractionRange = attractionRange;
		this.rewardsPoints = rewardsPoints;
	}

	public String getAttractionName() {
		return attractionName;
	}

	public void setAttractionName(String attractionName) {
		this.attractionName = attractionName;
	}

	public Double getAttractionLattitude() {
		return attractionLattitude;
	}

	public void setAttractionLattitude(Double attractionLattitude) {
		this.attractionLattitude = attractionLattitude;
	}

	public Double getAttractionLongitude() {
		return attractionLongitude;
	}

	public void setAttractionLongitude(Double attractionLongitude) {
		this.attractionLongitude = attractionLongitude;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Double getAttractionRange() {
		return attractionRange;
	}

	public void setAttractionRange(Double attractionRange) {
		this.attractionRange = attractionRange;
	}

	public int getRewardsPoints() {
		return rewardsPoints;
	}

	public void setRewardsPoints(int rewardsPoints) {
		this.rewardsPoints = rewardsPoints;
	}

}
